package exam01;

import java.util.List;

public class Prueba {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Articulo art1 = new Articulo("titulo1", "periodista", 1, "Prueba", 1);
		Articulo art2 = new Articulo("titulo2", "periodista", 2, "Prueba", 2);
		Articulo art3 = new Articulo("titulo3", "periodista", 3, "Prueba", 3);
		Articulo art4 = new Articulo("titulo4", "periodista", 4, "Prueba", 4);
	
		art1.addTema("Deportes");
		art2.addTema("Deportes");
		art3.addTema("Deportes");
		art4.addTema("Economia");
		
		Revista rev1 = new Revista("Rolling Stone", 4);
		rev1.addElemento(art1);
		rev1.addElemento(art2);
		rev1.addElemento(art3);
		rev1.addElemento(art4);
		
		System.out.println("La revista se llama: "+rev1.getTitulo()+" y el ejemplar es el numero: "+rev1.getEjemplar());
		System.out.println("Actualmente tiene los siguientes elementos: \n"+rev1.getElementos());
		System.out.println("Actualmente tiene los siguientes elementos: \n"+rev1.getCantidadElementos());
		System.out.println("El articulo: " +rev1.getArticuloEnPosicion(0)+ " existe...");
		System.out.println("El articulo: " +rev1.getArticuloEnPosicion(0)+ " existe...");
       
		System.out.println("La cantidad de revista con tema Deportes es: " + rev1.getCantidadArticulosDeTema("Deportes"));
		List<String> var = rev1.getElementos().get(0).getTemas();
		System.out.println(var);
	}

}
