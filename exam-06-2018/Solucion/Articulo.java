package exam01;

import java.util.ArrayList;
import java.util.List;

public class Articulo {

	private String titulo;
	private String periodista;
	private List<String> temas;
	private int palabras;
	private String texto;
	private long idArticulo;
	
	public Articulo(String titulo, String periodista, int palabras, String texto, long idArticulo) {
		super();
		this.titulo = titulo;
		this.periodista = periodista;
		this.palabras = palabras;
		this.texto = texto;
		this.idArticulo = idArticulo;
		this.temas = new ArrayList<String>();
	}

	
	public String getTitulo() {
		return titulo;
	}



	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}



	public String getPeriodista() {
		return periodista;
	}



	public void setPeriodista(String periodista) {
		this.periodista = periodista;
	}



	public List<String> getTemas() {
		return temas;
	}



	public void setTemas(List<String> temas) {
		this.temas = temas;
	}



	public int getPalabras() {
		return palabras;
	}



	public void setPalabras(int palabras) {
		this.palabras = palabras;
	}



	public String getTexto() {
		return texto;
	}



	public void setTexto(String texto) {
		this.texto = texto;
	}



	public long getIdArticulo() {
		return idArticulo;
	}



	public void setIdArticulo(long idArticulo) {
		this.idArticulo = idArticulo;
	}


	public Articulo() {
		// TODO Auto-generated constructor stub
	}


	public boolean addTema(String tema) {
		return temas.add(tema);
	}
	
	public boolean contieneTema(String tema) {
		return temas.contains(tema);
	}
}
